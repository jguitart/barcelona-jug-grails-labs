package jug.todolist

class AuthService {

    def authUser(String username, String password) {
		JugUser user = JugUser.findByUsername(username)
		boolean status = user && user.password == password;
		return [status:status, user:user]
    }
}
