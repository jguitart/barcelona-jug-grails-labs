<g:if test="${flash.error}">
	<div>${flash.error}</div>
</g:if>
<g:each in="${todoLists}" var="todoList" status="i">
	<g:if test="${i>0}">
	<span>|</span>
	</g:if>
	<span>
	<%--
		Remote link lanza una petición ajax y actualiza el elemento de html con el id indicado en 
		el atributo update.
		
		En el atributo onSuccess se llama a la función javascript indicada en caso que la llamada haya tenido éxito.
		En este caso se llama a una función remoteFunction.
		RemoteFunction es un tag (aquí reflejado en forma de función) que crea una función de llamada
		ajax. 
		En este caso realiza una llamada a la función list del controlador taskHandle para refrescar la lista
		de tareas.
	 --%>
		<g:remoteLink 
			url="[controller:'todoListHandle',action:'select', id:todoList.id]" 
			onSuccess="${remoteFunction(controller:'taskHandle',action:'list',update:'taskList')}">${todoList.name}</g:remoteLink>
	</span>
</g:each>