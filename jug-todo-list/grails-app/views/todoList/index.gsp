<html>
<%-- 
Estos dos tags nos necesarios para
que funcione ajax.
Lo que hacen es incluir jquery.
 --%>
<g:javascript library="jquery" />
<r:layoutResources/>
<body>
<%--
El tag formRemote realiza una llamada ajax a la url descrita por
'url' (a través de los parámetros controller, action y id) y que
actualiza el elemento con el id indicado en el atributo update.

Si no se quiere trabajar con html y se prefiere trabajar con datos
(a través de servicios rest y en formato json por ejemplo) se pueden usar los
atributos de evento onSuccess y onFail.
 --%>
<g:formRemote name="addTodoList" url="[controller:'todoListHandle',action:'add']" update="todoLists" >
<g:textField name="name" placeholder="new task list"/><g:submitButton name="addTodoListSubmit" value="add list"/>
</g:formRemote>
<div id="todoLists">
<%--
El tag include realiza un server-side include 
que permite incluir la llamada completa a un controlador.
 --%>
<g:include controller="todoListHandle" action="list"/>
</div>

<div id="taskListBlock">
	<div id="taskForm">
		<g:formRemote name="addTask" url="[controller:'taskHandle',action:'add']" update="taskList">
		<g:textField name="desc" placeholder="new task" /><g:submitButton name="addTaskSubmit" value="add task"/>
		</g:formRemote>
	</div>
	<div id="taskList">
		<g:include controller="taskHandle" action="list"/>
	</div>
</div>
</body>
</html>