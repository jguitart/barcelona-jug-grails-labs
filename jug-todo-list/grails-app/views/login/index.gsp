<g:if test="${flash.error}">
	<div id="loginError">
		<font color="red">${flash.error}</font>
	</div>
</g:if>
<div id="loginForm">
<g:form url="[controller:'login', action:'login']" method="POST" name="loginForm" useToken="true">
	<div id="username">
		<label for="username">Usuario:</label><g:textField name="username" id="username" placeholder="username"/>
	</div>
	<div id="password">
		<label for="password">Password:</label><g:textField name="password" id="password"/>
	</div>
	<div id="loginFormActions">
		<g:submitButton name="login" value="aceptar"/>
	</div>
</g:form>
</div> 