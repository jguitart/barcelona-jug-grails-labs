package jug.todolist

class LoginController {

	def authService
	
    def index() {
		//Simplemente muestra el fomulario de login.
	}
	
	//Función que realiza la autenticación
	def login() {
		def auth = authService.authUser(params.username, params.password)
		if(auth.status) {
			session.user = auth.user
			//Comprobamos si tiene la TodoList "main"
			TodoList main = TodoList.findOrCreateWhere(
				name:'main',
				user:session.user
				);
			
			if(!main.id) {
				main.save()
			}
			//Asignamos la lista a la sesión
			session.taskList = main
			
			//Redirigimos a todoList.
			//Si no se indica la action, ser llamará a la
			//action por defecto, en este caso "index".
			redirect(controller:'todoList')
		} else {
			//Asignamos el mensaje de error.
			flash.error = "Error en identificación"
			//Mostramos el formulario de nuevo para que se muestre el error
			//y el usuario pueda repetir la operación.
			render(view:'index')
		}
	}
	
	/**
	 * <p>Función de logout.</p>
	 * @return
	 */
	def logout() {
		session.user = null
		session.taskList = null
		redirect(action:'index')
	}
}
