package jug.todolist

class TodoListController {
	
	//Usamos el interceptor para comprobar el usuario.
	//Lo usamos como ejemplo, puesto que es mucho más seguro
	//usar filtros.
	
	def beforeInterceptor = {
		if(!session.user) {
			redirect(controller:'login', action:'index')
			return false
		}
	}
	
    def index() {
		List<TodoList> todoLists = TodoList.findAllByUser(session.user)
		def taskClause = Task.where {
			user == session.user
		} 
		def todoTasks = taskClause.list()
		[todoLists:todoLists, todoTasks:todoTasks]
	}
	
	
}
