package jug.todolist.handle

import org.apache.tools.ant.taskdefs.Parallel.TaskList;

import jug.todolist.Task;

class TaskHandleController {

    def list() {
		def clause = Task.where {
			user == session.user &&
			todoList == session.taskList &&
			done == false
		}
		List<Task> taskList = clause.list();
		render template:'list', model:[taskList:taskList]
	}
	
	def add() {
		Task task = new Task(params)
		task.user = session.user
		task.todoList = session.taskList
		task.save(validate:true)
		redirect action:'list'
	}
	
	def done() {
		Task task = Task.get(params.id)
		if(task) {
			task.done = true
			task.save()
		}
		redirect action:'list'
	}
}
