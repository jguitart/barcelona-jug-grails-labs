package jug.todolist.handle

import jug.todolist.TodoList;

class TodoListHandleController {
	
	def add() {
		TodoList newList = new TodoList(params)
		newList.user = session.user
		newList.save(validate:true)
		redirect(action:'list')
	}
	
	def list() {
		List<TodoList> todoLists = TodoList.findAllByUser(session.user)
		render template:'todoLists', model:[todoLists:todoLists]
	}
	
	def select() {
		TodoList taskList = TodoList.get(params.id)
		if(taskList) {
			session.taskList = taskList
		}
		render "ok"
	}
}
