import jug.todolist.JugUser
import jug.todolist.TodoList

class BootStrap {

    def init = { servletContext ->
		
		//A falta de un sistema de creación de usuarios
		//Creamos un usuario con el cual trabajar con esta demo.
		//Creamos el usuario y una lista por defecto.
		
		JugUser jugUser = JugUser.findOrCreateWhere(
				username:'jug',
				password:'jug'
			)
		jugUser.save()
		
		TodoList main = TodoList.findOrCreateWhere(
				name:'main',
				user:jugUser
			)
		main.save()
		
		
    }
    def destroy = {
    }
}
