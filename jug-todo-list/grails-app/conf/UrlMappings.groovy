class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		//Redirigimos la llamada raíz a la página principal
		"/"(controller:'todoList', action:'index')
		//Por comodidad, y hasta la llegada a producción,
		//mantendremos la vista de index.gsp con el listado de todos
		//los controladores en "/admin"
		"/admin"(view:"/index")
		"500"(view:'/error')
	}
}
