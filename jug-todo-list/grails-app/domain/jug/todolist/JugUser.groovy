package jug.todolist


class JugUser {
	
	//Nombre del usuario
	String username
	
	//Password
	String password
	
	//Campos de mantenimiento
	Date createDate;
	Date lastUpdate;
	
	//Interceptores de evento de persistencia.
	//Los utilizaremos para mantener los campos de mantenimiento de forma automática.
	def beforeInsert() {
		Date newDate = new Date()
		createDate = newDate;
		lastUpdate = newDate;
		
		//En este punto se podría aprovechar para codificar el password
		//Tan sólo hay que descomentar esta línea de código
		//password = password.encodeAsSHA1()
		
	}
	
	def beforeUpdate() {
		lastUpdate = new Date();
	}

    static constraints = {
		username blank:false
		password blank:false
		
		//Cómo que los campos de mantenimiento tendrán un comportamiento automático
		//dejamos que puedan ser null en la validación.
		createDate nullable:true
		lastUpdate nullable:true
    }
}
