package jug.todolist

class Task {
	
	//El campo id se definde de forma automática.
	//igualmente se genera y mantiene el campo versión version.
	
	//Descripción de la tarea
	String desc;
	
	//Campos de mantenimiento
	Date createDate;
	Date lastUpdate;
	
	JugUser user;
	TodoList todoList
	
	//Booleano que indica si la tarea está marcada como realizada
	boolean done;
	
	static belongTso = [todoList:TodoList,user:JugUser]
	
	//Interceptores de evento de persistencia.
	//Los utilizaremos para mantener los campos de mantenimiento de forma automática.
	def beforeInsert() {
		Date newDate = new Date()
		createDate = newDate;
		lastUpdate = newDate;
	}
	
	def beforeUpdate() {
		lastUpdate = new Date();
	}
	

    static constraints = {
		//Forzamos que la descripción esté informada
		desc blank:false
		
		//Cómo que los campos de mantenimiento tendrán un comportamiento automático
		//dejamos que puedan ser null en la validación.
		createDate nullable:true
		lastUpdate nullable:true
    }
}
