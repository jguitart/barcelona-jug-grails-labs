package jug.todolist

import java.util.Date;

class TodoList {
	
	//Nombre de la lista
	String name
	
	//Campos de mantenimiento
	Date createDate;
	Date lastUpdate;
	
	
	//Relación many-to-one de TodoList a jugUser y a todoList.
	//Resulta más eficiente de esta forma.
	static belongsTo = [user:JugUser]
	
	//Interceptores de evento de persistencia.
	//Los utilizaremos para mantener los campos de mantenimiento de forma automática.
	def beforeInsert() {
		Date newDate = new Date()
		createDate = newDate;
		lastUpdate = newDate;
	}
	
	def beforeUpdate() {
		lastUpdate = new Date();
	}

    static constraints = {
		name blank:false
		
		//Cómo que los campos de mantenimiento tendrán un comportamiento automático
		//dejamos que puedan ser null en la validación.
		createDate nullable:true
		lastUpdate nullable:true
    }
}
